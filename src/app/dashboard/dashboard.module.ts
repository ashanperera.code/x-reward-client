import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { ScoreComponent } from './score/score.component';
import { AccountHistoryComponent } from './account-history/account-history.component';
import { SalesInformationComponent } from './sales-information/sales-information.component';
import { OrderInformationComponent } from './order-information/order-information.component';
import { NoticeBoardComponent } from './notice-board/notice-board.component';


@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    UserCreateComponent,
    ScoreComponent,
    AccountHistoryComponent,
    SalesInformationComponent,
    OrderInformationComponent,
    NoticeBoardComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class DashboardModule { }
