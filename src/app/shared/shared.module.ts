import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MenuItemsComponent } from './menu-items/menu-items.component';



@NgModule({
  declarations: [
    ToolbarComponent,
    MenuItemsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
